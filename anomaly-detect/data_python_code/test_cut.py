import json
import datetime

import time
import pandas as pd

#경로
path = ".\data/"
#파일명
#파일리스트
filelist = ["action-2021.03.01.json", "action-2021.03.02.json"]
#while
max = 1000000
count = 0
speed = 1
minute = 60 / speed
for filename in filelist:
    #open
    try:
        f = open(path+filename, 'r')
    except:
        print('no file')
    else:
        #print(path+filename)
        last_min = -1
        #while
        while True:
           
            #readline
            line = f.readline()
            count += 1
            if (count > max) :
                break
            if not line : break
            #print(line)
            #string to json
            str_json = json.loads(line)
            #print(json.dumps(str_json, indent=2))
            #timestamp key 값 확인(_source/log date)
            timestamp = str_json["_source"]["@timestamp"]
            #print(timestamp)
            #string -> timedate 변환
            date_var = datetime.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S.%fZ')
            
           # print(date_var)
           
            #date_time = date_var.strftime('%Y-%m-%dT%H:%M:%S')
            #print("date and time:", date_time)
            #print(type(date_var))
            #데이터 타임에서 cur_min으로 구한다.
            if last_min != date_var.minute:
                if last_min == -1:
                    print("date and time:", date_var.minute) 
                    last_min = date_var.minute
                else:
                    time.sleep(minute)    
                    print("date and time:", date_var.minute) 
                    last_min = date_var.minute
            #last_min과  cur_min과 다르면 sleep
            #print("date and time:", date_var.minute)

            #write(json)
            with open(".\data/datetime.json","a") as json_file:
                json_file.writelines(line)
                
                #print(line)

        #close
        f.close()