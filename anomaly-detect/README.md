# KDT-20 이상감지

# 프로젝트 참가자 
- 윤성훈 : yoonhun92@naver.com
- 유연경 : dusrud0911@gmail.com
- 양재옥 : yangjo0109@gmail.com
- 이지민 : minghgh@naver.com


# 폴더구성
- anomaly-detect-1
    - 이상감지 구현
- data-pipeline
    - 시스템 구축용 설정 파일
- data-python_code
    - 데이터 전처리 관련 python code
- flink-streaming
    - flink-streaming 관련 파일 및 폴더
- ppt
    - 발표 자료 정리
- sample
    - 참고용 샘플 파일
- simulator
    - data-loader.py 
- wordcount
    - flink wordcount sample
    