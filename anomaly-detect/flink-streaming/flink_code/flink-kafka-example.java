final Properties properties = new Properties();

properties.setProperty("bootstrap.servers", config.getProperties().get("kafka.bootstrap.servers"));
properties.setProperty("group.id", config.getProperties().get("kafka.group.id"));

final FlinkKafkaConsumer<String> kafkaSource = new FlinkKafkaConsumer<String>(
        config.getProperties().get("kafka.topic"), new SimpleStringSchema(), properties);

stream = env.addSource(kafkaSource);
//여기까지가 kafka 연동


env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
WatermarkStrategy<ChatLogItem> wmStrategy = WatermarkStrategy
    .<ChatLogItem>forBoundedOutOfOrderness(Duration.ofMillis(watermarkMilliseconds))
    .withTimestampAssigner((event, timestamp) -> event.getEpoch_timestamp());
    
DataStream<Tuple5<String, String, String, Long, Long>> withTimestamp = stream.map(new String2LogItem()) 
    .assignTimestampsAndWatermarks(wmStrategy)
    .filter(new FilterFunction<ChatLogItem>() {
        private static final long serialVersionUID = 5302468281720605463L;
        @Override
        public boolean filter(ChatLogItem value) throws Exception {    
            if(value.getContentProviderKey() == null) return false;
            return true;
        }
    })
    .map(new MapFunction<ChatLogItem, Tuple5<String, String, String, Long, Long>>() { 
            private static final long serialVersionUID = -3215122648966391472L;

            @Override
            public Tuple5<String, String, String, Long, Long> map(ChatLogItem value) throws Exception {
                return Tuple5.of(
                    value.getContentProviderKey(),
                    value.getContentProviderKey() + "-" + value.getBroadcast_key(),
                    value.getMethod(),
                    1L,
                    value.getEpoch_timestamp());
            }
    })
    .keyBy(value -> value.f1 + "-" + value.f2) 
    .timeWindow(Time.milliseconds(windowSizeMilliseconds)).sum(3); 
