

final Properties properties = new Properties();
properties.setProperty("bootstrap.servers","kafka.bootstrap.servers");
//bootstrap server : kafka server로 바꾸기
properties.setProperty("group.id", "kafka.group.id");
//group id 는 우리가 임의로 정한 group_id >>근데 이게 maven에서 설정하는 group_id 인지 궁금 
final FlinkKafkaConsumer<String> kafkaSource = new FlinkKafkaConsumer<String>(
        "kafka.topic", new SimpleStringSchema(), properties);
//kafka에서 정한 topic_id >> kafka.topic
// kafkaSource.setCommitOffsetsOnCheckpoints(true); // (by default, the behaviour is true)
// kafkaSource.setStartFromEarliest();

stream = env.addSource(kafkaSource);
// 위 코드로 변환시켜서 쓰면 그 때 말씀하신 sorket 변경



env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
//우리가 사용하는 time은 eventTime임. 헷갈리지 말 것
WatermarkStrategy<ChatLogItem> wmStrategy = WatermarkStrategy
    .<ChatLogItem>forBoundedOutOfOrderness(Duration.ofMillis(watermarkMilliseconds))
    //초당 들어오는 속도로, 몇초 당 보낼건지 들어오는 속도 이상 값을 넣어줘야함. 안그러면 데이터가 죽어버림
    .withTimestampAssigner((event, timestamp) -> event.getEpoch_timestamp());
    
DataStream<Tuple5<String, String, String, Long, Long>> withTimestamp = stream.map(new String2LogItem()) //string2logitem으로 맵핑
    .assignTimestampsAndWatermarks(wmStrategy)s
    .filter(new FilterFunction<ChatLogItem>() { //형변환은 안되지만, 필터로 고쳐줄 수 있음. 아직 string2logitem
        private static final long serialVersionUID = 5302468281720605463L;
        @Override
        public boolean filter(ChatLogItem value) throws Exception {    
            if(value.getContentProviderKey() == null) return false;
            return true;
        }
    })
    .map(new MapFunction<ChatLogItem, Tuple5<String, String, String, Long, Long>>() { //chatLogItem으로 형변환
            private static final long serialVersionUID = -3215122648966391472L;

            @Override
            public Tuple5<String, String, String, Long, Long> map(ChatLogItem value) throws Exception {
                return Tuple5.of(
                    value.getContentProviderKey(),
                    value.getContentProviderKey() + "-" + value.getBroadcast_key(),
                    value.getMethod(),
                    1L,
                    value.getEpoch_timestamp());
            }
    })
    .keyBy(value -> value.f1 + "-" + value.f2) //앞 두개 키를 묶어 그룹화 시켜줌. 
    .timeWindow(Time.milliseconds(windowSizeMilliseconds)).sum(3); 
    //그룹화 된 애들의 인덱스 3번째의 값을 sum해서 나타냄. 위에서 넣어준 1L이 hit이고 hit을 더했다고 생각하면 될 듯
