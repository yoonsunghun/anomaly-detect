package com.sample;

import java.time.Duration;
import java.util.Properties;

import com.google.gson.Gson;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
@Slf4j

public class StreamingJob {

    private static DataStream<Hit> filterStream;

    public static void main(String... args) throws Exception {

        // Checking input parameters
        final ParameterTool params = ParameterTool.fromArgs(args);

        Configuration flinkConfig = new Configuration();
        StreamExecutionEnvironment env = null;

        // flinkConfig.setString("taskmanager.memory.network.fraction", "1.0");
        // flinkConfig.setString("taskmanager.memory.network.min", "512mb");
        // flinkConfig.setString("taskmanager.memory.network.max", "512mb");
        // env = StreamExecutionEnvironment.createLocalEnvironment(1, flinkConfig);
        // } else {
        env = StreamExecutionEnvironment.getExecutionEnvironment();
        // }

        
        env.getConfig().setGlobalJobParameters(params);

        // 동작하지 않음
        // System.setProperty("taskmanager.memory.network.min", "64m");
        // System.setProperty("taskmanager.memory.network.max", "1g");

        DataStream<String> stream = null;

        // nc -l -p 9090

        //stream = env.socketTextStream("127.0.0.1", 9090, "\n");
        final Properties properties = new Properties();

        String kafkaHost = "kafka:9092";
        String inputTopicName = "data005";
        String outputTopicName = "data005_1";
        String groupId = "catenoid01";

        properties.setProperty("bootstrap.servers", kafkaHost);
        properties.setProperty("group.id", groupId);
        final FlinkKafkaConsumer<String> kafkaSource = new FlinkKafkaConsumer<String>(
                inputTopicName, new SimpleStringSchema(), properties);
        //kafkaSource.setStartFromEarliest();
        stream = env.addSource(kafkaSource);
        
        // Gson gson = new Gson();
        // Hit member = gson.fromJson(str, Hit.class);
        DataStream<Hit> hitStream = stream.map(new RichMapFunction<String,Hit>() {

            private static final long serialVersionUID = 1L;
            private Gson gson;

            @Override
            public Hit map(String value) throws Exception {
                Hit member = gson.fromJson(value, Hit.class);
                //gson.toJson(member);
                return member;
            }

            @Override
            public void open(Configuration parameters) throws Exception {
                gson = new Gson();
            }
        
            @Override
            public void close() throws Exception {}

        });

        // hitStream.print();

        long watermarkMilliseconds = 1000;
        WatermarkStrategy<Hit> wmStrategy = WatermarkStrategy
            .<Hit>forBoundedOutOfOrderness(Duration.ofMillis(watermarkMilliseconds))
            .withTimestampAssigner((event, timestamp) -> event.Epoch_timestamp());
        
        
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime); 

        filterStream = hitStream.filter(new FilterFunction<Hit>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public boolean filter(Hit value) throws Exception {
                if(value.getCpk() == null) return false;
                //if(value.getCpk().compareTo("stc-kollus")!=0) return false; //가져오고싶은 cpk만 추출
                return true;
            }

        })
        .assignTimestampsAndWatermarks(wmStrategy);
        //filterStream.print();

        DataStream<Tuple3<String, String, Long>> mapstream = filterStream.map(new MapFunction<Hit, Tuple3<String, String, Long>>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Tuple3<String, String, Long> map(Hit value) throws Exception {
                return Tuple3.of(value.getTimestamp(), value.getCpk(), 1L);
            }
        });

        //mapstream.print();
        
        long windowSizeseconds =60; 
        DataStream<Tuple3<String, String, Long>> tuplestream = mapstream
            .keyBy(value -> value.f1)
            .timeWindow(Time.seconds(windowSizeseconds)).sum(2); 
        
        //     .keyBy(value -> value.f1 + "-" + value.f2) 
        //     .timeWindow(Time.milliseconds(windowSizeMilliseconds)).sum(3); 


        //tuplestream.print();
        DataStream<String> stringstream = tuplestream.map(new RichMapFunction<Tuple3<String, String, Long>, String>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public String map(Tuple3<String, String, Long> value) throws Exception {
                return String.format("{\"timestamp\":\"%s\", \"cpk\":\"%s\", \"hit\":\"%d\"} ", value.f0, value.f1, value.f2);
//                return String.format("{\"timestamp\":\"%f\"} ", value.f2);
            }
        });

        //stringstream.print();

        FlinkKafkaProducer<String> www = new FlinkKafkaProducer<String>(
                outputTopicName,
                new SimpleStringSchema(),
                properties);
                stringstream.addSink(www);

        //filterStream.assignTimestampsAndWatermarks(
        //    WatermarkStrategy.forBoundedOutOfOrderness(Duration.ofMillis(watermarkMilliseconds)));
        //env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);

        //long watermarkMilliseconds = 1000;
        // WatermarkStrategy<String> wmStrategy = WatermarkStrategy
        //     .<String>forBoundedOutOfOrderness(Duration.ofMillis(watermarkMilliseconds))
        //     .withTimestampAssigner((event, timestamp) -> event.getEpoch_timestamp());
        
        // DataStream<Tuple5<String, String, String, Long, Long>> withTimestamp = stream.map(new String2LogItem()) 
        //     .assignTimestampsAndWatermarks(wmStrategy)
        //     .filter(new FilterFunction<ChatLogItem>() {
        //         private static final long serialVersionUID = 5302468281720605463L;
        //         @Override
        //         public boolean filter(ChatLogItem value) throws Exception {    
        //             if(value.getContentProviderKey() == null) return false;
        //             return true;
        //         }
        //     })
        //     .map(new MapFunction<ChatLogItem, Tuple5<String, String, String, Long, Long>>() { 
        //             private static final long serialVersionUID = -3215122648966391472L;

        //             @Override
        //             public Tuple5<String, String, String, Long, Long> map(ChatLogItem value) throws Exception {
        //                 return Tuple5.of(
        //                     value.getContentProviderKey(),
        //                     value.getContentProviderKey() + "-" + value.getBroadcast_key(),
        //                     value.getMethod(),
        //                     1L,
        //                     value.getEpoch_timestamp());
        //             }
        //     })
        //     .keyBy(value -> value.f1 + "-" + value.f2) 
        //     .timeWindow(Time.milliseconds(windowSizeMilliseconds)).sum(3); 

        env.execute("flink");
    }
}