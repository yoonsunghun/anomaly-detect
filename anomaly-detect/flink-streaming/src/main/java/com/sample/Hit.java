package com.sample;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;

@Data
public class Hit {
    //@SerializedName(value="hit")
    //private String hit;
    @SerializedName(value="timestamp")
    private String timestamp;
    @SerializedName(value="cpk")
    private String cpk;

    public long Epoch_timestamp() {
        try {
        Date d = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(this.timestamp);
        long date2long = d.getTime();
        return date2long;
        } catch (Exception e) {
            return -1;
        }
    }
}
