package com.sample;


import com.google.gson.Gson;

public class GsonSample {
    
    public static void main(String[] args) {
        
        //System.out.println("hi");
        //String str = "{'timestamp':'2021-03-01T00:03:24.000Z','cpk':'stc-kollus','@index':'vgrender'}";// String형으로 변환
        String str = "{'cpk':'stc-kollus','@index':'vgrender'}";// String형으로 변환
        //String strJson = br.readLine();
        Gson gson = new Gson();
        Hit member = gson.fromJson(str, Hit.class);
        //System.out.println(member.getIn());
        //System.out.println(member.getCpk());
        //System.out.println(member.getTimestamp());
    }

}