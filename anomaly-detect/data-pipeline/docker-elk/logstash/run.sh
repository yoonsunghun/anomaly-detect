#!/bin/bash

IMAGE=logstash-with-influxdb:0.1

docker stop logstash

docker run -it --rm \
    --network dockerelk_default \
    -v $(pwd)/config/logstash.yml:/usr/share/logstash/config/logstash.yml \
    -v $(pwd)/pipeline:/usr/share/logstash/pipeline \
    -v /home/adminuser/notebooks/datadrive/data:/usr/share/logstash/data/ \
    --name logstash \
    ${IMAGE}
