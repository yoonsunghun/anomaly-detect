# # data load
import json
import datetime
import time
import os

#경로
path = "../../notebooks/datadrive/data/new/export/"

#파일명
#파일리스트
#filelist = ["action-2021.02.22.json","action-2021.02.23.json","action-2021.02.26.json","action-2021.02.27.json"]
#filelist = ["action-2021.02.15.json","action-2021.02.16.json","action-2021.02.17.json","action-2021.02.18.json","action-2021.02.19.json","action-2021.02.20.json","action-2021.02.21.json","action-2021.02.22.json","action-2021.02.23.json","action-2021.02.24.json","action-2021.02.25.json","action-2021.02.26.json","action-2021.02.27.json","action-2021.02.28.json","action-2021.03.01.json","action-2021.03.02.json","action-2021.03.03.json","action-2021.03.04.json","action-2021.03.05.json","action-2021.03.06.json","action-2021.03.07.json"]
#filelist = ["action-2021.03.08.json","action-2021.03.09.json","action-2021.03.10.json","action-2021.03.11.json","action-2021.03.12.json","action-2021.03.13.json","action-2021.03.14.json"]
filelist = ["action-2021.03.09.json"]
#while
#max = 300000
#count1 = 0
count = -1
speed = 60
minute = 60 / speed
newfilepath="../../notebooks/datadrive/data/"
for filename in filelist:
    #open
    try:
        f = open(path+filename, 'r')
        newfile = "streaming_"+filename
        newfileopen = newfilepath+newfile
        count+=1
    except:
        print('no file')
    else:
        #print(path+filename)
        last_min = -1
        #while
        while True:
            line = f.readline()
            if not line : break    
            str_json = json.loads(line)
            timestamp = str_json["_source"]["@timestamp"]
            date_var = datetime.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S.%fZ')

            if last_min != date_var.minute:
                if last_min == -1:
                    print("date and time:", date_var.minute)
                    last_min = date_var.minute
                else:
                    time.sleep(minute)
                    print("date and time:", date_var.minute)
                    last_min = date_var.minute

            with open(newfileopen,"a") as json_file:
                json_file.writelines(line)

        f.close()
        removecount=count-3
        if removecount>=0:
            removefilepath=newfilepath+"streaming_"+filelist[removecount]
            os.remove(removefilepath)
