package com.sample;

import java.util.Properties;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;

import lombok.extern.slf4j.Slf4j;

@Slf4j

public class StreamingJob {

    public static void main(String... args) throws Exception {

        // Checking input parameters
        final ParameterTool params = ParameterTool.fromArgs(args);

        Configuration flinkConfig = new Configuration();
        StreamExecutionEnvironment env = null;

        // flinkConfig.setString("taskmanager.memory.network.fraction", "1.0");
        // flinkConfig.setString("taskmanager.memory.network.min", "512mb");
        // flinkConfig.setString("taskmanager.memory.network.max", "512mb");
        //env = StreamExecutionEnvironment.createLocalEnvironment(1, flinkConfig);
        // } else {
            env = StreamExecutionEnvironment.getExecutionEnvironment();
        // }

        env.getConfig().setGlobalJobParameters(params);

        // 동작하지 않음
        // System.setProperty("taskmanager.memory.network.min", "64m");
        // System.setProperty("taskmanager.memory.network.max", "1g");

        DataStream<String> stream = null;

        // nc -l -p 9090
        
        //stream = env.socketTextStream("127.0.0.1", 9090, "\n");
        final Properties properties = new Properties();

        properties.setProperty("bootstrap.servers", "kafka:9092");
        properties.setProperty("group.id", "catenoid");

        final FlinkKafkaConsumer<String> kafkaSource = new FlinkKafkaConsumer<String>(
                "action01", new SimpleStringSchema(), properties);
        kafkaSource.setStartFromEarliest();
        stream = env.addSource(kafkaSource);
        
        //env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);

        // long watermarkMilliseconds = 1000;
        // WatermarkStrategy<String> wmStrategy = WatermarkStrategy
        //     .<String>forBoundedOutOfOrderness(Duration.ofMillis(watermarkMilliseconds))
        //     .withTimestampAssigner((event, timestamp) -> event.getEpoch_timestamp());

        // DataStream<String> stream_String = stream.
            
        stream.print();        
        /**
         * execute
         */
        env.execute("word count");
    }

}