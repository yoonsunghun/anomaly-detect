## INPUT
1. __raw data__
- 어떤 필터링도 거치지 않은 데이터 형태

|#|Column|Dtype|Example|
|:---:|:---:|:---:|:---|
|0|_index|object|action-2021.03.01|
|1|_type|object|useractionlog|
|2|_id|object|AXfrFau5iVteQFMsMhm0|
|3|_score|float64|NaN|
|4|_source|object|{'@fields': {'action': 'v', 'method': 'vgrende...|
|5|sort|object|[1614556800000]|

![이미지](/anomaly-detect-1/01_raw.jpg "raw_data")

<br>

2. __logstash 이후 data__
- logstash filter를 통해 1차 전처리 된 데이터
- 집계에 필요한 vgrender, cpk, timestamp를 저장

|#|Column|Dtype|Example|
|:---:|:---:|:---:|:---|
|0|method|object|vgrender|
|1|cpk|object|stc-kollus|
|2|\@timestamp|object|2021-03-01T00:00:00.000Z|

![이미지](/anomaly-detect-1/02_logstash.jpg "after_logstash")

<br>

3. __flink 이후 data__
- 모델링 하기 위해 필요한 최종 input data 형태
- 시간(분)당 cpk 별 vgrender 합계 값이 저장되어 있는 형태
- 데이터에 저장된 cpk 수에 따라 저장되는 데이터 열의 수 달라짐

|#|Column|Dtype|Example|
|:---:|:---:|:---:|:---|
|0|\@timestamp|datetime|2021-03-01 00:00:00|
|1|cpk_1|object|140, 128, 101, ...|
|2|cpk_2|object|19, 12, 5, ...|
|3|cpk_3|object|2, 4, 8, ...|
|4|cpk_4|object|39, 48, 56, ...|
|5|cpk_5|object|568, 591, 603, ...|

![이미지](/anomaly-detect-1/03_flink.jpg "after_flink")

<br><br>

## OUTPUT
- 모델링을 거친 후 influxDB에 저장될 데이터 형태
- (210330 현재) cpk 별로 이상치가 발견된 timestamp와 해당 이상치 값이 저장된 데이터프레임

|#|Column|Dtype|Example|
|:---:|:---:|:---:|:---|
|0|@timestamp|datetime|2021-03-01 07:26:00|
|1|cpk_name|object|1000|

![이미지](/anomaly-detect-1/04_modeling.jpg "output")