## 데이터 분석을 위한 모듈(pandas, matplotlib, train_test_split) 불러오기
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

from keras.layers import LSTM 
from keras.models import Sequential 
from keras.layers import Dense, Dropout
import keras.backend as K 
from keras.callbacks import EarlyStopping

# 데이터 불러오기 - pandas의 read_csv 함수 이용
data = pd.read_csv('./test3.csv')# 전처리된 상태의 1개의 고객 회사 완전 가상 데이터 test 파일 불러오기 

# data의 @timestamp 열 데이트 타입 형식으로 변환
data['@timestamp'] = pd.to_datetime(data['@timestamp'])
# @timestamp 열 인덱스로 설정 및 기존 @timestamp 열 삭제
data.index = data['@timestamp']
data=data.drop(['@timestamp'],axis=1)

# cpk 이름을 리스트에 저장
# 리스트에서 하나씩 받아서 df 생성.. >> 이걸 변수로 가져올 수 있나...
cpk_col =  list(data.columns)   # 데이터프레임 생성 위한 열 목록(cpk 이름들)
cpk_df = list()                 # 생성된 각각의 데이터프레임이 저장될 리스트
cpk_model = list()
for cpk in cpk_col:
  temp = data[cpk].values
  globals()['df_{}'.format(cpk)] = pd.DataFrame(temp, columns=[cpk], index=data.index)
  cpk_df.append(globals()['df_{}'.format(cpk)])

for df in cpk_df:
  # print(df.head())
  # col = str(df.columns.values)
  # print(col)
  # 3월1일 6시 기준으로 train, test 분리
  split_date = pd.Timestamp('2021-03-01 06:00:00') # train, test 나눌 기준 지정
  train = data.loc[:split_date, df.columns] # train set 지정 처음부터 split_date 까지
  test = data.loc[split_date:, df.columns] # test set 지정 split_date 부터 끝까지
  
  # ax = train.plot() # train, test set 분포Timestamp('2021-03-01 06:00:00') # train, test 나눌 기준 지정
  # test.plot(ax=ax)
  # plt.legend(['train', 'test'])

  scaler = MinMaxScaler()
  train_scaled = scaler.fit_transform(train)
  test_scaled = scaler.fit_transform(test)
  
  # print(train_scaled)

  # 정규화된 train, test 데이터로 dataframe 생성
  train_sc_df = pd.DataFrame(train_scaled, columns=[df.columns], index=train.index)
  test_sc_df = pd.DataFrame(test_scaled, columns=[df.columns], index=test.index)
  # print(train_sc_df.head(5))

  # sliding window 구성
  for s in range(1, 61):
    train_sc_df['shift_{}'.format(s)] = train_sc_df[df.columns].shift(s)
    test_sc_df['shift_{}'.format(s)] = test_sc_df[df.columns].shift(s)
    
  # print(train_sc_df.head())


  # X_train, X_test에서 NaN있는 행 제거 후 y로 설정할 값 제거
  X_train = train_sc_df.dropna().drop(df.columns, axis=1)
  y_train = train_sc_df.dropna()[[df.columns]]
  X_test = test_sc_df.dropna().drop(df.columns, axis=1)
  y_test = test_sc_df.dropna()[[df.columns]]
  
  # test에서 validation 값 분리(test 0.8, validation 0.2)
  X_test,X_val, y_test, y_val = train_test_split(X_test, y_test,
                                                 test_size = 0.2,
                                                 random_state = 3)
  
  # print(X_train.head())
  # dataframe 타입이었던 훈련/테스트 데이터들을 values 를 사용하여 dataframe의 numpy 표현형으로 변경
  # print(type(X_train))
  # # print(type(X_train))
  # print(X_train.shape)
  # print(y_train.shape)
  # print(X_val.shape)

  X_train = X_train.values
  X_test= X_test.values
  y_train = y_train.values
  y_test = y_test.values
  X_val = X_val.values
  y_val = y_val.values


  # LSTM의 input으로 사용하기 위해 2차원인 X_train, X_test, X_val 값을 3차원으로 수정
  # [data_size, time_steps, features]
  X_train_t = X_train.reshape(X_train.shape[0], 60, 1)
  X_test_t = X_test.reshape(X_test.shape[0], 60, 1)
  X_val_t = X_val.reshape(X_val.shape[0], 60, 1)
  
  # print("최종 DATA")
  # print(X_train_t.shape)
  
  
  K.clear_session()
  
  # 상태유지 순환신경망 모델 생성
  # ‘stateful=True’옵션을 사용하여 상태유지 가능한 순환신경망 모델을 구성
  model = Sequential() # Sequeatial Model 
  model.add(LSTM(32, batch_input_shape=(1, 60, 1), stateful=True)) # batch_input_shape = (배치사이즈, 타임스텝, 속성) 
  model.add(Dense(1)) # output = 1 
  model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])
  model.summary()

  early_stop = EarlyStopping(monitor='loss', patience=1, verbose=1)
  history = model.fit(X_train_t, y_train, 
                      validation_data = (X_val_t, y_val), 
                      epochs=50, shuffle=False,
                      batch_size=1, verbose=1,
                      callbacks=[early_stop])
  # 모델 평가
  train_acc = model.evaluate(X_train_t, y_train, batch_size=1, verbose=0)
  test_acc = model.evaluate(X_test_t,y_test, batch_size=1, verbose=0)
  
  print(train_acc)
  print(test_acc)
  # 모델 평가
  #acc
  plt.plot(history.history['accuracy']) #train set
  plt.plot(history.history['val_accuracy'])

  plt.title('Accuracy')
  plt.xlabel('epoch')
  plt.ylabel('acc')
  plt.legend(['train', 'val'], loc='upper left')
  plt.show()
  
  #loss
  plt.plot(history.history['loss']) #train set
  plt.plot(history.history['val_loss'])
  
  plt.title('Loss')
  plt.xlabel('epoch')
  plt.ylabel('acc')
  plt.legend(['train', 'val'], loc='upper left')
  plt.show()


  y_pred = model.predict(X_test_t, batch_size=1)
  plt.figure(figsize=(8, 5))
  plt.plot(y_test, label='actual')
  plt.plot(y_pred, label='prediction')
  plt.legend()
  plt.show()