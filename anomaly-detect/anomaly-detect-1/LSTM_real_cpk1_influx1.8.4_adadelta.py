## 데이터 분석을 위한 모듈(pandas, matplotlib, train_test_split) 불러오기

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import RobustScaler, MinMaxScaler, StandardScaler, MaxAbsScaler, Normalizer
from keras.layers import LSTM, SimpleRNN, GRU
from keras.models import Sequential, Model
from keras.models import load_model
from keras.layers import Dense, Dropout, Flatten, Input
from keras.layers.merge import concatenate 
import keras.backend as K 
from keras.callbacks import EarlyStopping
from influxdb import InfluxDBClient
import datetime
from datetime import timezone
import os
import sys

#start_date, end_date를 주기적으로 받는 방법?, cron?

#start_date = sys.argv[1]+' '+sys.argv[2]
start_date = sys.argv[1]+' 00:00:00'
print('1.'+start_date)
#end_date = sys.argv[2]+' '+sys.argv[4]
end_date = sys.argv[2]+' 23:59:59'
print('2.'+end_date)
cpk = sys.argv[3]
print('3.'+cpk)
host = "40.117.168.127"
port = "8086"
username = ""
password = ""
database = "data01_1"
_format = '%Y-%m-%d %H:%M:%S'
second = 1000000000

def convert_unixtime (start_date, end_date):
    start_date = datetime.datetime.strptime(start_date, _format)
    start_date_unix = start_date.replace(tzinfo=timezone.utc).timestamp()*second
    start_date_unix = int(start_date_unix)    
    start_date_str = str(start_date_unix)
    
    end_date = datetime.datetime.strptime(end_date, _format)
    end_date_unix = end_date.replace(tzinfo=timezone.utc).timestamp()*second
    end_date_unix = int(end_date_unix) 
    end_date_str = str(end_date_unix)

    #print(start_date_unix, end_date_unix)
    return start_date_str, end_date_str

def learning (start_date, end_date, cpk) :
    # print(start, end, cpk)
    # return 

    # 학습기 input 원하는 cpk,  과거 3일치 데이터를 불러와야함. 

    client = InfluxDBClient(host, port, username, password, database)
    query = 'SELECT * FROM "logstash" WHERE "cpk"=\''+ cpk +  '\' and '  + start_date + '<= "time" and "time" <=' + end_date
    print(query)
    result = client.query(query)
    row_lst = list(result.get_points())
    data = pd.DataFrame(row_lst)
    print(data)

    #fdf = data.copy()

    cpk = data.iloc[0, 1]
    print(cpk)

    # data의 @timestamp 열 데이트 타입 형식으로 변환
    # @timestamp 열 인덱스로 설정 및 기존 @timestamp 열 삭제
    data=data.rename(columns={'time':'@timestamp', 'hit':cpk})
    data=data.drop(['cpk'], axis=1) # cpk 여러 개 할 때 수정 필요 pd.pivot_table 필요 예상
    data['@timestamp'] = pd.to_datetime(data['@timestamp'])
    data.index = data['@timestamp']
    data=data.drop(['@timestamp'],axis=1)
    print(data)

    # train : test = 7 : 3 비율로 데이터 분할
    # train, test = train_test_split(data, test_size=0.3, random_state=1, shuffle=False)
    train = data

    # 딥러닝 모델이 학습을 잘하기 위해서는 정규화 해주는 작업이 필요
    # 각 Feature의 값을 일정한 범위 또는 규칙에 따르게 하기 위해서 스케일링 사용
    # RobustScaler :    중앙값(median)과 IQR(interquartile range) 사용. 아웃라이어의 영향을 최소화
    scaler = RobustScaler()
    # scaler = MinMaxScaler()
    # scaler = StandardScaler()
    # scaler = MaxAbsScaler()
    # scaler = Normalizer()

    train_scaled = scaler.fit_transform(train)

    # 정규화된 train, test 데이터로 dataframe 생성
    train_sc_df = pd.DataFrame(train_scaled, columns=[cpk], index=train.index)
    #print(train_sc_df.head())

    # sliding window 구성
    # window는 LSTM을 훈련하기 위한 단위로 고정된 사이즈를 가진다.
    # dataframe의 shift 연산을 사용하여 인덱스는 그대로 두고 데이터만 window 단위만큼 이동시킴
    # train, test 둘다 적용
    for s in range(1, 61):
        train_sc_df['shift_{}'.format(s)] = train_sc_df[cpk].shift(s)

    #print(train_sc_df.head())

    # X_train, X_test에서 NaN있는 행 제거 후 y로 설정할 값 제거
    X_train = train_sc_df.dropna().drop(cpk, axis=1)
    y_train = train_sc_df.dropna()[[cpk]]

    # test에서 validation 값 분리(test 0.8, validation 0.2)
    # X_test,X_val, y_test, y_val = train_test_split(X_test, y_test,
                                                # test_size = 0.2,
                                                # random_state = 3)

    # index = y_test.index
    index = y_train.index
    #print(X_train.head())
    # print(y_train.head())

    # dataframe 타입이었던 훈련/테스트 데이터들을 values 를 사용하여 dataframe의 numpy 표현형으로 변경
    # print(type(X_train))
    X_train = X_train.values
    # print(type(X_train))
    y_train = y_train.values
    # X_val = X_val.values
    # y_val = y_val.values
    print(X_train.shape)
    #print(X_train)
    #print(y_train.shape)
    #print(y_train)

    # LSTM을 사용하기 위해서는 3차원 데이터가 필요
    # LSTM의 input으로 사용하기 위해 2차원인 X_train, X_test, X_val 값을 3차원으로 수정
    # [data_size, time_steps, features]
    X_train_t = X_train.reshape(X_train.shape[0], 10, 1)
    # X_val_t = X_val.reshape(X_val.shape[0], 60, 1)

    #print("최종 DATA")
    #print(X_train_t.shape)

    K.clear_session()

    # 상태유지 순환신경망 모델 생성
    # 순환신경망 모델과 동일하나 ‘stateful=True’옵션을 사용하여 상태유지 가능한 순환신경망 모델을 구성
    # 상태유지 모드일 경우 한 배치에서 학습된 상태가 다음 배치 학습 시에 전달되는 방식
    # 학습 샘플의 가장 마지막 상태가 다음 샘플 학습 시에 입력으로 전달 여부를 지정하는 것입니다. 
    # 도출된 현재 상태의 가중치가 다음 샘플 학습 시의 초기 상태로 입력됨을 알 수 있습니다.
    input_tensor_1 = Input(shape=(10, 1))
    hidden_layers_1 = LSTM(64, return_sequences=True)(input_tensor_1)
    hidden_layers_1 = LSTM(64)(hidden_layers_1)
    # mp1 = MaxPool1D(padding='same')(hidden_layers_1)
    # ap1 = AveragePooling1D(padding='same')(hidden_layers_1)
    # concat1 = concatenate([mp1, ap1])
    hidden_layers_1 = Dense(128)(hidden_layers_1)
    hidden_layers_1 = Dense(256)(hidden_layers_1)
    hidden_layers_1 = Dense(256)(hidden_layers_1)
    hidden_layers_1 = Dense(128)(hidden_layers_1)
    hidden_layers_1 = Dense(64)(hidden_layers_1)
    output_tensor_1 = Dense(1)(hidden_layers_1)


    # 두번째 모델
    # input_tensor_2 = Input(shape=(60, 1))
    hidden_layers_2 = GRU(64, return_sequences=True)(input_tensor_1)
    hidden_layers_2 = GRU(64)(hidden_layers_2)
    # mp2 = MaxPool1D(padding='same')(hidden_layers_2)
    # ap2 = AveragePooling1D(padding='same')(hidden_layers_2)
    # concat2 = concatenate([mp2, ap2])
    hidden_layers_2 = Dense(128)(hidden_layers_2)
    hidden_layers_2 = Dense(256)(hidden_layers_2)
    hidden_layers_2 = Dense(256)(hidden_layers_2)
    hidden_layers_2 = Dense(128)(hidden_layers_2)
    hidden_layers_2 = Dense(64)(hidden_layers_2)
    output_tensor_2 = Dense(1)(hidden_layers_2)

    merged_model = concatenate([output_tensor_1, output_tensor_2])

    middle_1 = Dense(64)(merged_model)
    middle_2 = Dense(32)(middle_1)
    output = Dense(1)(middle_2)

    model = Model(inputs=[input_tensor_1], outputs=output) 

    model.compile(loss='mean_squared_error', optimizer='adadelta', metrics=['accuracy']) 
    model.summary()
    # 오버피팅(과적합)이 일어나는 acc최고점, loss최저점 확인을 위한 early_stop 코드
    early_stop = EarlyStopping(monitor='loss', patience=1, verbose=1)

    history = model.fit(X_train_t, y_train, 
                        # validation_data = (X_val_t, y_val), 
                        epochs=10, shuffle=False,
                        batch_size=32,
                        verbose=1,
                        callbacks=[early_stop])

    #end_date_learning(end_date) 를 저장
    lst = list()
    lst.append({
                        "measurement": "end_date_learning",
                        "tags": {
                            "cpk": cpk,
                        },
                        "fields": {"end_date_learning": end_date}
                        })
    client.write_points(lst)
    return model.save("model_{}.h5".format(cpk))


def anomaly (start_date, end_date, cpk) :
    # 참고용 모델을 어떻게 불러올것인지
    # 테스트 셋 불러와야함.
    #print(start_date, end_date)
    if os.path.isfile("model_{}.h5".format(cpk)) :
        print("Yes File")
        #start_date, end_date = convert_unixtime(start_date, end_date)

        model = load_model("model_{}.h5".format(cpk))

        client = InfluxDBClient(host, port, username, password, database)
        query = 'SELECT * FROM "logstash" WHERE "cpk"=\''+ cpk +  '\' and '  + start_date + '<= "time" and "time" <=' + end_date
        #print(query)
        result = client.query(query)
        row_lst = list(result.get_points())
        data = pd.DataFrame(row_lst)
        #print(data)
        #print(len(data))

        # 학습기의 end_date 를 가져옴
        query1 = 'SELECT end_date_learning FROM "end_date_learning" WHERE "cpk"=\''+ cpk + '\' ORDER BY desc LIMIT 1'
        print(query1)
        result1 = client.query(query1)
        print(result1)        
        row_lst1 = list(result1.get_points())
        print(row_lst1[0]['end_date_learning'])
        edl = row_lst1[0]['end_date_learning']

        if (len(data)== 0):
            print('No Test Data')
            return

        #fdf = data.copy()


        # data의 @timestamp 열 데이트 타입 형식으로 변환
        # @timestamp 열 인덱스로 설정 및 기존 @timestamp 열 삭제
        data=data.rename(columns={'time':'@timestamp', 'hit':cpk})
        data=data.drop(['cpk'], axis=1) # cpk 여러 개 할 때 수정 필요 pd.pivot_table 필요 예상
        data['@timestamp'] = pd.to_datetime(data['@timestamp'])
        data.index = data['@timestamp']
        data=data.drop(['@timestamp'],axis=1)
        print(data)
    
        # train : test = 7 : 3 비율로 데이터 분할
        # train, test = train_test_split(data, test_size=0.3, random_state=1, shuffle=False)
        test = data
        test_lst = data[cpk].tolist()

        # 딥러닝 모델이 학습을 잘하기 위해서는 정규화 해주는 작업이 필요
        # 각 Feature의 값을 일정한 범위 또는 규칙에 따르게 하기 위해서 스케일링 사용
        # RobustScaler :    중앙값(median)과 IQR(interquartile range) 사용. 아웃라이어의 영향을 최소화
        scaler = RobustScaler()
        # scaler = MinMaxScaler()
        # scaler = StandardScaler()
        # scaler = MaxAbsScaler()
        # scaler = Normalizer()

        test_scaled = scaler.fit_transform(test)

        # 정규화된 train, test 데이터로 dataframe 생성
        test_sc_df = pd.DataFrame(test_scaled, columns=[cpk], index=test.index)
        print(test_sc_df.head())

        # sliding window 구성
        # window는 LSTM을 훈련하기 위한 단위로 고정된 사이즈를 가진다.
        # dataframe의 shift 연산을 사용하여 인덱스는 그대로 두고 데이터만 window 단위만큼 이동시킴
        # train, test 둘다 적용
        for s in range(1, 11):
            test_sc_df['shift_{}'.format(s)] = test_sc_df[cpk].shift(s)

        print(test_sc_df.head())

        # X_train, X_test에서 NaN있는 행 제거 후 y로 설정할 값 제거
        X_test = test_sc_df.dropna().drop(cpk, axis=1)
        y_test = test_sc_df.dropna()[[cpk]]

        # test에서 validation 값 분리(test 0.8, validation 0.2)
        # X_test,X_val, y_test, y_val = train_test_split(X_test, y_test,
                                                    # test_size = 0.2,
                                                    # random_state = 3)

        # index = y_test.index
        index = y_test.index
        print(X_test.head())
        # print(y_train.head())

        # dataframe 타입이었던 훈련/테스트 데이터들을 values 를 사용하여 dataframe의 numpy 표현형으로 변경
        # print(type(X_train))
        X_test = X_test.values
        # print(type(X_train))
        y_test = y_test.values
        # X_val = X_val.values
        # y_val = y_val.values
        #print(X_train.shape)
        #print(X_train)
        #print(y_train.shape)
        #print(y_train)

        # LSTM을 사용하기 위해서는 3차원 데이터가 필요
        # LSTM의 input으로 사용하기 위해 2차원인 X_train, X_test, X_val 값을 3차원으로 수정
        # [data_size, time_steps, features]
        X_test_t = X_test.reshape(X_test.shape[0], 10, 1)
        # X_val_t = X_val.reshape(X_val.shape[0], 60, 1)
        y_pred = model.predict(X_test_t, batch_size=1)
        print(y_pred)
        y_pred_org = scaler.inverse_transform(y_pred)   # 정규화된 값을 원래의 크기로 되돌린다.
        print(y_pred_org)
        # print(test_lst)
        # y_val = list()
        # for i in range(len(y_pred)):
        #   y_val.append(y_pred[i])

        print("y_test", y_test.shape)
        print("y_pred", y_pred.shape)
        # print("y_val", y_val.shape)

        y_total = 0
        y_ok = 0
        ## 표본의 통계량(estimator) 추정
        mean_y_pred = np.mean(y_pred_org)
        median_y_pred = np.median(y_pred_org)
        std_y_pred = np.std(y_pred_org)
        print(std_y_pred)

        ## 상한, 하한 추정(95% 구간 z=1.645)
        ## 상한, 하한 추정(95% 구간 z=1.96)
        ## 상한, 하한 추정(99% 구간 z=2.58)
        z_score = 2.58*(std_y_pred/np.sqrt(len(y_pred_org)))
        # z_score = 1.645*std_y_pred
        L_ = mean_y_pred - z_score
        U_ = mean_y_pred + z_score
        print("L_ ", L_)
        print("U_ ", U_)
        print("z_score ", z_score)


        pred_lst = list()
        for n in range(len(y_pred)):
            y_total += 1
            y_min = y_pred_org[n] - 3*z_score
            y_max = y_pred_org[n] + 3*z_score
            if (test_lst[n] > y_min and test_lst[n] < y_max):
                y_ok += 1
            else:
                pred_lst.append([str(data[cpk].index[n]), float(test_lst[n])])
        print("y_total: ", y_total, " y_ok: ", y_ok)
        acc = y_ok / y_total
        print("acc: ", acc)

        plt.figure(figsize=(10, 7))
        plt.plot(y_test, marker='o', markersize='0.5', label='actual')
        plt.plot(y_pred, marker='o', markersize='0.5', label='prediction')
        plt.legend()
        plt.show()
        plt.savefig('model_ensemble.png')

        pred_dt = pd.DataFrame(pred_lst, columns=['@timestamp', 'actual_value'])
        pred_dt['@timestamp'] = pd.to_datetime(pred_dt['@timestamp'], utc=True)
        pred_dt = pred_dt.set_index('@timestamp')

        anomaly_df = pd.concat([pred_dt, data], join='outer', axis=1).dropna()
        anomaly_df = anomaly_df.drop('actual_value', axis=1)
        anomaly_df = anomaly_df.reset_index()
        print(anomaly_df)

        """
        @@ Write
        python --> Influxdb
        """
        lst = list()
        for n in range(len(anomaly_df)):
            ts = anomaly_df.iloc[n][0]
            #print(ts)
            ts2 = convert_unixtime(ts)
            #print(ts2)
            #print(edl)
            val = int(anomaly_df.iloc[n][1])
            if(int(start_date)<=int(edl)):
                #print(edl)
                if (ts2>=edl):
                    print(ts, val)
                    lst.append({
                        "measurement": "anomaly",
                        "tags": {
                            "cpk": cpk,
                        },
                        "time": ts,
                        "fields": {"anomaly": val}
                        })
                else:
                    continue
            else:
                lst.append({
                    "measurement": "anomaly",
                    "tags": {
                        "cpk": cpk,
                    },
                    "time": ts,
                    "fields": {"anomaly": val}
                    })

        client.write_points(lst)

    else :
        print("No file")
        return


start_date = convert_unixtime(start_date)
end_date = convert_unixtime(end_date)
print(start_date, end_date)
if(int(end_date) - int(start_date) == learning_standard*ns):
    print("Learning Function Start!")
    learning(start_date, end_date, cpk)
#learning(start_date, end_date, cpk)

print("Anomaly Start, End Date Change")
tmp = end_date
start_date = str(int(tmp)-60*45*ns)
end_date = str(int(tmp)+60*15*ns)

print(start_date, end_date)
anomaly(start_date, end_date, cpk)
