#!/bin/bash

#echo "CRON TEST" >> /var/log/cron-test.txt

start_date=$(date +%F -d '2 months ago 9 days ago')
echo $start_date
end_date=$(date +%F -d '2 months ago 4 days ago')
echo $end_date
#python3 /mnt/c/git/anomaly-detect-1/test.py $start_date $end_date
python3 /mnt/c/git/anomaly-detect-1/test.py $start_date $end_date >> /mnt/c/git/anomaly-detect-1/output.txt
