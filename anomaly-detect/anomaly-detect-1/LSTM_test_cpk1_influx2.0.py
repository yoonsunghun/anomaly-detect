## 데이터 분석을 위한 모듈(pandas, matplotlib, train_test_split) 불러오기
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import RobustScaler, MinMaxScaler, StandardScaler, MaxAbsScaler, Normalizer
from keras.layers import LSTM 
from keras.models import Sequential 
from keras.layers import Dense, Dropout
import keras.backend as K 
from keras.callbacks import EarlyStopping
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
from datetime import datetime, timedelta

token = "lD15Q_iK8s4LtXA9NoA8FZXQXVCl6F8sevrQuXT3ppjhkd5T4YbKGzr9_X5m9gGNf8PJKIr9nl-4i5rl4Qnnng=="
org = "a"
bucket_input = "test11"
bucket_output = "test18"

client = InfluxDBClient(url="http://localhost:28086", token=token, org = org)
write_api = client.write_api(write_options=SYNCHRONOUS)
query_api = client.query_api()

# 데이터 불러오기 - pandas의 read_csv 함수 이용
data = query_api.query_data_frame('from(bucket:"'+bucket_input+'") '
                                        '|> range(start: -1000000m) ' # 처음부터 하는 방법.
                                        '|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value") '
                                        '|> keep(columns: ["@timestamp", "cpk", "hit"])')
print(data.to_string())
print(type(data))
print(data)
"""
Close client
"""
client.close()
data = data.sort_values(by=['cpk','@timestamp'], ascending=[True, True])
data=data.drop(['result','table'], axis=1)
data=data.rename(columns={'hit':'dreamcom'})
data=data.drop(['cpk'], axis=1) # cpk 여러 개 할 때 수정 필요 pd.pivot_table 필요 예상
data2 = data.copy() #write 테스트 용도

# data의 @timestamp 열 데이트 타입 형식으로 변환
# @timestamp 열 인덱스로 설정 및 기존 @timestamp 열 삭제
data['@timestamp'] = pd.to_datetime(data['@timestamp'])
data3 = data.copy() #write 테스트 용도

print('test')
print(type(data3.values[0][0]))

data.index = data['@timestamp']
data=data.drop(['@timestamp'], axis=1)



print(data.head())
print(data.info())  # data 정보 확인
# data.plot() # data 분포 확인

# train : test = 7 : 3 비율로 데이터 분할
train, test = train_test_split(data, test_size=0.3, random_state=1, shuffle=False)
print(train.head())
# ax = train.plot()
# test.plot(ax=ax)
# plt.legend(['train', 'test'])


# 딥러닝 모델이 학습을 잘하기 위해서는 정규화 해주는 작업이 필요
# 각 Feature의 값을 일정한 범위 또는 규칙에 따르게 하기 위해서 스케일링 사용
# # RobustScaler :    중앙값(median)과 IQR(interquartile range) 사용. 아웃라이어의 영향을 최소화
scaler = RobustScaler()
# # scaler = MinMaxScaler()
# # scaler = StandardScaler()
# # scaler = MaxAbsScaler()
# # scaler = Normalizer()

train_scaled = scaler.fit_transform(train)
test_scaled = scaler.fit_transform(test)

print(train_scaled)
print(test_scaled)


# # 정규화된 train, test 데이터로 dataframe 생성
train_sc_df = pd.DataFrame(train_scaled, columns=['dreamcom'], index=train.index)
test_sc_df = pd.DataFrame(test_scaled, columns=['dreamcom'], index=test.index)
print(train_sc_df.head())



# # sliding window 구성
# # window는 LSTM을 훈련하기 위한 단위로 고정된 사이즈를 가진다.
# # dataframe의 shift 연산을 사용하여 인덱스는 그대로 두고 데이터만 window 단위만큼 이동시킴
# # train, test 둘다 적용
for s in range(1, 61):
    train_sc_df['shift_{}'.format(s)] = train_sc_df['dreamcom'].shift(s)
    test_sc_df['shift_{}'.format(s)] = test_sc_df['dreamcom'].shift(s)

print(train_sc_df.head())



# # X_train, X_test에서 NaN있는 행 제거 후 y로 설정할 값 제거
X_train = train_sc_df.dropna().drop('dreamcom', axis=1)
y_train = train_sc_df.dropna()[['dreamcom']]
X_test = test_sc_df.dropna().drop('dreamcom', axis=1)
y_test = test_sc_df.dropna()[['dreamcom']]

# test에서 validation 값 분리(test 0.8, validation 0.2)
X_test,X_val, y_test, y_val = train_test_split(X_test, y_test,
                                              test_size = 0.2,
                                              random_state = 3)

index = y_test.index
print(X_train.head())
# # print(y_train.head())



# # dataframe 타입이었던 훈련/테스트 데이터들을 values 를 사용하여 dataframe의 numpy 표현형으로 변경
print(type(X_train))
X_train = X_train.values
print(type(X_train))
X_test= X_test.values
y_train = y_train.values
y_test = y_test.values
X_val = X_val.values
y_val = y_val.values
print(X_train.shape)
#print(X_train)
print(y_train.shape)
#print(y_train)

print(X_val.shape)



# LSTM을 사용하기 위해서는 3차원 데이터가 필요
# LSTM의 input으로 사용하기 위해 2차원인 X_train, X_test, X_val 값을 3차원으로 수정
# [data_size, time_steps, features]
X_train_t = X_train.reshape(X_train.shape[0], 60, 1)
X_test_t = X_test.reshape(X_test.shape[0], 60, 1)
X_val_t = X_val.reshape(X_val.shape[0], 60, 1)

print("최종 DATA")
print(X_train_t.shape)





K.clear_session()

# 상태유지 순환신경망 모델 생성
# 순환신경망 모델과 동일하나 ‘stateful=True’옵션을 사용하여 상태유지 가능한 순환신경망 모델을 구성
# 상태유지 모드일 경우 한 배치에서 학습된 상태가 다음 배치 학습 시에 전달되는 방식
# 학습 샘플의 가장 마지막 상태가 다음 샘플 학습 시에 입력으로 전달 여부를 지정하는 것입니다. 
# 도출된 현재 상태의 가중치가 다음 샘플 학습 시의 초기 상태로 입력됨을 알 수 있습니다.
model = Sequential() # Sequeatial Model 
model.add(LSTM(32, batch_input_shape=(1, 1, 1), stateful=False)) # batch_input_shape = (배치사이즈, 타임스텝, 속성)  
model.add(Dense(32))
model.add(Dropout(0.3))
model.add(Dense(32))
model.add(Dropout(0.3))
model.add(Dense(32))
model.add(Dropout(0.3))
model.add(Dense(32))
model.add(Dropout(0.3))
model.add(Dense(32))
model.add(Dropout(0.3))
model.add(Dense(1)) # output = 1 

# loss : 최적화 과정에서 최소화될 손실 함수(loss function)를 설정 - mse 사용 (정답에 대한 오류를 숫자로 나타내는 것, 정답에 가까울수록 작은 값)
# optimizer : 훈련 과정을 설정 - adam 사용
# metrics : 훈련을 모니터링하기 위해 사용 - accuracy 확인
model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy']) 
model.summary()



# 오버피팅(과적합)이 일어나는 acc최고점, loss최저점 확인을 위한 early_stop 코드
early_stop = EarlyStopping(monitor='loss', patience=1, verbose=1)

history = model.fit(X_train_t, y_train, 
                    validation_data = (X_val_t, y_val), 
                    epochs=50, shuffle=False,
                    batch_size=1,
                    verbose=1,
                    callbacks=[early_stop])

model.save("model.h5")


# 모델 평가
train_acc = model.evaluate(X_train_t, y_train, batch_size=1, verbose=0)
test_acc = model.evaluate(X_test_t,y_test, batch_size=1, verbose=0)

print(train_acc)
print(test_acc)

# #acc
# plt.plot(history.history['accuracy']) #train set
# plt.plot(history.history['val_accuracy'])

# plt.title('Accuracy')
# plt.xlabel('epoch')
# plt.ylabel('acc')
# plt.legend(['train', 'val'], loc='upper left')
# plt.show()

# #loss
# plt.plot(history.history['loss']) #train set
# plt.plot(history.history['val_loss'])

# plt.title('Loss')
# plt.xlabel('epoch')
# plt.ylabel('acc')
# plt.legend(['train', 'val'], loc='upper left')
# plt.show()



y_pred = model.predict(X_test_t, batch_size=1)
print(y_pred)
plt.figure(figsize=(8, 5))
plt.plot(y_test, marker='o', label='actual')
plt.plot(y_pred, marker='o', label='prediction')
plt.legend()
plt.show()


# data_df = list()
# 이상치가 발견된 timestamp만을 추출하여 df형태로 저장
pred_lst = list()
for n in range(len(y_pred)):
  diff = abs(y_pred[n] - y_test[n])
  if (diff > 1):
    # print(y_test[n])
    # print(index[n])
    pred_lst.append([str(index[n]), float(y_test[n])])

pred_dt = pd.DataFrame(pred_lst, columns=['@timestamp', 'actual_value'])
pred_dt['@timestamp'] = pd.to_datetime(pred_dt['@timestamp'])
pred_dt = pred_dt.set_index('@timestamp')

anomaly_df = pd.concat([pred_dt, data], join='outer', axis=1).dropna()
anomaly_df = anomaly_df.drop('actual_value', axis=1)
anomaly_df = anomaly_df.reset_index()
# data_df.append(anomaly_df)
print(anomaly_df)

anomaly_col = list(anomaly_df.columns) # a[0] = timestamp / a[1] = 'dreamcom'
anomaly_val = anomaly_df.values # 2차원 배열로 b[0][0] = timestamp의 값. b[0][1] 드림컴의 이상치 값

data_col = list(data2.columns)
print(data_col[1])
data_test_val = data2.values
data_test2_val = data3.values
print(type(data_test_val[2][0]))
print(data_test2_val[3][1])
print(len(data_test2_val))
print(type(data_test2_val[1][0]))
# # print(data_time_val[0])
# # data_cpk_val = data2['dreamcom'].values
# # print(data_cpk_val[0])


"""
Prepare data
"""
# 이상치 값 write
for i in range(len(anomaly_val)):
#   point = Point("mem").tag("cpk", anomaly_col[1]).tag(anomaly_col[0], anomaly_val[i][0]).field("outlier", anomaly_val[i][1])
#   write_api.write(bucket_output, org, point)

  point = Point("mem20").tag("cpk", anomaly_col[1]).field("outlier", anomaly_val[i][1]).time(anomaly_val[i][0], WritePrecision.NS)
  write_api.write(bucket_output, org, point)

#500개 write
# for i in range(0, 499):
#   point = Point("mem").tag("cpk", data_col[1]).tag(data_col[0], data_test_val[i][0]).field("hit", data_test_val[i][1])
#   write_api.write(bucket, org, point)


# 데이트 타입으로 변환한 timestamp 넣어서 오류는 생기지 않지만, influxdb에 write 되지 않음.
# for i in range(len(data_test2_val)):
#   point = Point("mem8").tag("cpk", data_col[1]).field("hit", data_test2_val[i][1]).time(data_test2_val[i][0]-timedelta(hours = 9), WritePrecision.NS)
#   write_api.write(bucket_output, org, point)
# test1 = Point("my_measurement").tag(a[0], b[0][0],tag(a[1], b[0][1])
# Point("mem").tag("host", "host1").field("used_percent", 23.43234543).time(datetime.utcnow(), WritePrecision.NS)

# write_api.write(bucket=bucket, record=[test1])