#!/bin/bash
#start_date_learning=$(date -d '2 months ago 16 days ago' "+%Y-%m-%d")
start_date_learning=$(date -d '2 months ago 16 days ago' "+%Y-%m-%d %H:%M:%S")
echo $start_date_learning
#end_date=$(date -d '1 months ago 26 days ago' "+%Y-%m-%d")
end_date=$(date -d '1 months ago 26 days ago' "+%Y-%m-%d %H:%M:%S")
echo $end_date
#python3 /mnt/c/git/anomaly-detect-1/test.py $start_date_learning $end_date
python3 /mnt/c/git/anomaly-detect-1/code/learning.py $start_date_learning $end_date >> /mnt/c/git/anomaly-detect-1/output.txt