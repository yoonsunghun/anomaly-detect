import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import RobustScaler, MinMaxScaler, StandardScaler, MaxAbsScaler, Normalizer
from keras.layers import LSTM, SimpleRNN, GRU
from keras.models import Sequential, Model
from keras.models import load_model
from keras.layers import Dense, Dropout, Flatten, Input
from keras.layers.merge import concatenate 
import keras.backend as K 
from keras.callbacks import EarlyStopping
from influxdb import InfluxDBClient
import datetime
from datetime import timezone
import os
import sys
import redis
import time

# 1. InfluxDB cpk list 획득 및 1개씩 뽑기 + 다 뽑아서 학습이 끝나는 거 까지 확인 필요 (cron 주기에 있는)
# 리스트 획득 함수
start_date = sys.argv[1] + ' ' + sys.argv[2]
print('1.'+start_date)
#end_date = sys.argv[2]+' '+sys.argv[4]
end_date = sys.argv[3] + ' ' + sys.argv[4]
print('2.'+end_date)
host_influxdb = "40.117.168.127"
port_influxdb = "8086"
username_influxdb = ""
password_influxdb = ""
database_influxdb = "data001_1"
_format = '%Y-%m-%d %H:%M:%S'
host_redis='localhost'
port_redis=6379
db_redis=0
ns = 1000000000
period = 60*1
learning_standard = 60*60*24*3-1

def convert_datetime (date):
    if(type(date)==str):
        date = datetime.datetime.strptime(date, _format) # datetype 변경
    date_unix = date.replace(tzinfo=timezone.utc).timestamp()*ns # KST > UTC 변경, unixtime으로 변경
    date_unix = int(date_unix) # 지수 형태 > 전체 표기로 표기 형식 변경
    date_str = str(date_unix) # InfluxDB에서 사용하기 위해 다시 string으로 변경

    #print(start_date_unix, end_date_unix)
    return date_str # unixtime string 으로 리턴

def cpk_list_get (end_date):
    # 인플럭스 디비에서 cpk list만 가져오는 쿼리
    # 리스트에 저장
    client = InfluxDBClient(host_influxdb, port_influxdb, username_influxdb, password_influxdb, database_influxdb)
    #query = 'SHOW TAG values WITH KEY = "cpk" LIMIT 1000 OFFSET 1'
    query = 'SELECT * FROM "logstash" WHERE '+ str((int(end_date) - period*ns)) + '<= "time" and "time" <=' + end_date
    print(query)
    result = client.query(query)
    print(result)
    if (len(result) == 0) :
        print("No Cpk")
        quit()
    row_lst = pd.DataFrame(result.get_points())
    cpk_list = row_lst['cpk'].to_list()
    # print(row_lst[0]['value'])
    # print(len(row_lst))
    # cpk_list = []
    # for i in row_lst:
    #     cpk_list.append(i['value'])
    cpk_list = list(set(cpk_list))
    return cpk_list

def process () :
    rd = redis.StrictRedis(host_redis, port_redis, db_redis, decode_responses=True)
    cnt = rd.get("cnt_learning")
    print('count ' + cnt)
    if cnt < '5' :
        return 
    else :
        print("sleep")
        time.sleep(10)
        process()

def learning (start_date, end_date, cpk) :
    
    history_write = list()

    print('learning '+ start_date + ' ' + end_date)

    # 데이터 양 고려 필요?
    # print(start, end, cpk)
    # return 
    # 학습기 5일 데이터 확인하는 코드 필요

    # 학습기 input 원하는 cpk,  과거 3일치 데이터를 불러와야함.

    if (int(end_date) - int(start_date) < learning_standard*ns):
        return
    
    client = InfluxDBClient(host_influxdb, port_influxdb, username_influxdb, password_influxdb, database_influxdb)
    query = 'SELECT sum("hit") FROM "logstash" WHERE ("cpk"=\''+ cpk +  '\') and '  + start_date + '<= "time" and "time" <=' + end_date + ' GROUP BY time(1m) fill(0)'
    print(query)
    result = client.query(query)
    row_lst = list(result.get_points())
    data = pd.DataFrame(row_lst)
    time = data['time'].iloc[-1]
    print ('time ' + time)
    print(data)
    print(len(data))
    #fdf = data.copy()
    

    history_write.append({
    "measurement": "history",
    "tags": {
        "cpk": cpk,
    },
    "time": time ,
    "fields": {"cpk": cpk}
    })
    client.write_points(history_write)

    # data의 @timestamp 열 데이트 타입 형식으로 변환
    # @timestamp 열 인덱스로 설정 및 기존 @timestamp 열 삭제
    data=data.rename(columns={'time':'@timestamp', 'sum':cpk})
    data['@timestamp'] = pd.to_datetime(data['@timestamp'])
    data.index = data['@timestamp']
    data=data.drop(['@timestamp'],axis=1)
    print(data)
    # train : test = 7 : 3 비율로 데이터 분할
    # train, test = train_test_split(data, test_size=0.3, random_state=1, shuffle=False)
    train = data

    # 딥러닝 모델이 학습을 잘하기 위해서는 정규화 해주는 작업이 필요
    # 각 Feature의 값을 일정한 범위 또는 규칙에 따르게 하기 위해서 스케일링 사용
    # RobustScaler :    중앙값(median)과 IQR(interquartile range) 사용. 아웃라이어의 영향을 최소화
    scaler = RobustScaler()
    # scaler = MinMaxScaler()
    # scaler = StandardScaler()
    # scaler = MaxAbsScaler()
    # scaler = Normalizer()

    train_scaled = scaler.fit_transform(train)

    # 정규화된 train, test 데이터로 dataframe 생성
    train_sc_df = pd.DataFrame(train_scaled, columns=[cpk], index=train.index)
    print(train_sc_df.head())

    # sliding window 구성
    # window는 LSTM을 훈련하기 위한 단위로 고정된 사이즈를 가진다.
    # dataframe의 shift 연산을 사용하여 인덱스는 그대로 두고 데이터만 window 단위만큼 이동시킴
    # train, test 둘다 적용
    for s in range(1, 11):
        train_sc_df['shift_{}'.format(s)] = train_sc_df[cpk].shift(s)

    print(train_sc_df.head())

    # X_train, X_test에서 NaN있는 행 제거 후 y로 설정할 값 제거
    X_train = train_sc_df.dropna().drop(cpk, axis=1)
    y_train = train_sc_df.dropna()[[cpk]]

    # test에서 validation 값 분리(test 0.8, validation 0.2)
    # X_test,X_val, y_test, y_val = train_test_split(X_test, y_test,
                                                # test_size = 0.2,
                                                # random_state = 3)

    # index = y_test.index
    index = y_train.index
    #print(X_train.head())
    # print(y_train.head())

    # dataframe 타입이었던 훈련/테스트 데이터들을 values 를 사용하여 dataframe의 numpy 표현형으로 변경
    # print(type(X_train))
    X_train = X_train.values
    # print(type(X_train))
    y_train = y_train.values
    # X_val = X_val.values
    # y_val = y_val.values
    print(X_train.shape)
    #print(X_train)
    #print(y_train.shape)
    #print(y_train)

    # LSTM을 사용하기 위해서는 3차원 데이터가 필요
    # LSTM의 input으로 사용하기 위해 2차원인 X_train, X_test, X_val 값을 3차원으로 수정
    # [data_size, time_steps, features]
    X_train_t = X_train.reshape(X_train.shape[0], 10, 1)
    # X_val_t = X_val.reshape(X_val.shape[0], 60, 1)

    print("최종 DATA")
    print(X_train_t.shape)

    K.clear_session()

    # 상태유지 순환신경망 모델 생성
    # 순환신경망 모델과 동일하나 ‘stateful=True’옵션을 사용하여 상태유지 가능한 순환신경망 모델을 구성
    # 상태유지 모드일 경우 한 배치에서 학습된 상태가 다음 배치 학습 시에 전달되는 방식
    # 학습 샘플의 가장 마지막 상태가 다음 샘플 학습 시에 입력으로 전달 여부를 지정하는 것입니다. 
    # 도출된 현재 상태의 가중치가 다음 샘플 학습 시의 초기 상태로 입력됨을 알 수 있습니다.
    input_tensor_1 = Input(shape=(10, 1))
    hidden_layers_1 = LSTM(64, return_sequences=True)(input_tensor_1)
    hidden_layers_1 = LSTM(64)(hidden_layers_1)
    # mp1 = MaxPool1D(padding='same')(hidden_layers_1)
    # ap1 = AveragePooling1D(padding='same')(hidden_layers_1)
    # concat1 = concatenate([mp1, ap1])
    hidden_layers_1 = Dense(128)(hidden_layers_1)
    hidden_layers_1 = Dense(256)(hidden_layers_1)
    hidden_layers_1 = Dense(256)(hidden_layers_1)
    hidden_layers_1 = Dense(128)(hidden_layers_1)
    hidden_layers_1 = Dense(64)(hidden_layers_1)
    output_tensor_1 = Dense(1)(hidden_layers_1)


    # 두번째 모델
    # input_tensor_2 = Input(shape=(60, 1))
    hidden_layers_2 = GRU(64, return_sequences=True)(input_tensor_1)
    hidden_layers_2 = GRU(64)(hidden_layers_2)
    # mp2 = MaxPool1D(padding='same')(hidden_layers_2)
    # ap2 = AveragePooling1D(padding='same')(hidden_layers_2)
    # concat2 = concatenate([mp2, ap2])
    hidden_layers_2 = Dense(128)(hidden_layers_2)
    hidden_layers_2 = Dense(256)(hidden_layers_2)
    hidden_layers_2 = Dense(256)(hidden_layers_2)
    hidden_layers_2 = Dense(128)(hidden_layers_2)
    hidden_layers_2 = Dense(64)(hidden_layers_2)
    output_tensor_2 = Dense(1)(hidden_layers_2)

    merged_model = concatenate([output_tensor_1, output_tensor_2])

    middle_1 = Dense(64)(merged_model)
    middle_2 = Dense(32)(middle_1)
    output = Dense(1)(middle_2)

    model = Model(inputs=[input_tensor_1], outputs=output) 

    model.compile(loss='mean_squared_error', optimizer='adadelta', metrics=['accuracy']) 
    model.summary()
    # 오버피팅(과적합)이 일어나는 acc최고점, loss최저점 확인을 위한 early_stop 코드
    early_stop = EarlyStopping(monitor='loss', patience=1, verbose=0)

    history = model.fit(X_train_t, y_train, 
                        # validation_data = (X_val_t, y_val), 
                        epochs=10, shuffle=False,
                        batch_size=32,
                        verbose=1,
                        callbacks=[early_stop])

    #end_date_learning(end_date) 를 저장
    lst = list()
    lst.append({
                        "measurement": "end_date_learning",
                        "tags": {
                            "cpk": cpk,
                        },
                        "fields": {"end_date_learning": end_date}
                        })
    client.write_points(lst)
    return model.save("/mnt/c/git/anomaly-detect-1/model_{}.h5".format(cpk))



start_date = convert_datetime (start_date)
end_date = convert_datetime (end_date)
cpk_list = cpk_list_get (end_date)
print(cpk_list)
#print(a)

rd = redis.StrictRedis(host_redis, port_redis, db_redis, decode_responses=True)

process()
rd.get("cnt_learning")
rd.incr("cnt_learning") # redis +1
for i in cpk_list:
    client = InfluxDBClient(host_influxdb, port_influxdb, username_influxdb, password_influxdb, database_influxdb)
    query = 'SELECT cpk FROM "history" WHERE time>'+ str(int(end_date) - 14*60*60*24*ns) +' and time<'+end_date
    print(query)
    result = client.query(query)

    if (len(result) == 0) :
        print("No history")
        learning(start_date, end_date, i)
    else : 
        row_lst = pd.DataFrame(result.get_points())
        history_list = row_lst['cpk'].to_list()
        print(history_list)
        if i in history_list :
            print(i)
            print("Yes history")
            continue
        else :
            print("No history")
            learning(start_date, end_date, i)
            
rd.decr("cnt_learning")