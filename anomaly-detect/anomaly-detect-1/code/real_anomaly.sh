#!/bin/bash
#end_date=$(date -d '1 months ago 26 days ago' "+%Y-%m-%d")
end_date=$(date -d '1 months ago 26 days ago' "+%Y-%m-%d %H:%M:%S")
echo $end_date
#start_date_anomaly=$(date -d '1 months ago 26 days ago 10 minutes ago' "+%Y-%m-%d")
start_date_anomaly=$(date -d '1 months ago 26 days ago 10 minutes ago' "+%Y-%m-%d %H:%M:%S")
echo $start_date_anomaly
python3 /mnt/c/git/anmoaly-detect-1/code/anomaly.py $start_date_anomaly $end_date >> /mnt/c/git/anomaly-detect-1/output.txt