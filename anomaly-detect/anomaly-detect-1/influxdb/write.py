from datetime import datetime

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

# You can generate a Token from the "Tokens Tab" in the UI
token = "SAb_WS8scMjlYu1liSBOFwjmSdPyLSo9B0VrQ8sL_Path4rAZ6aPFOaYahost32FQqKZmU4__EggagfsG9HcwA=="
org = "admin"
bucket = "sunghunyoon"

client = InfluxDBClient(url="http://172.26.16.108:28086", token=token)

write_api = client.write_api(write_options=SYNCHRONOUS)

data = "mem,host=host1 used_percent=23.43234543"
write_api.write(bucket, org, data)

point = Point("mem").tag("host", "host1").field("used_percent", 23.43234543).time(datetime.utcnow(), WritePrecision.NS)

write_api.write(bucket, org, point)

sequence = ["mem,host=host1 used_percent=23.43234543",
            "mem,host=host1 available_percent=15.856523"]
write_api.write(bucket, org, sequence)

query = f'from(bucket: \\"{bucket}\\") |> range(start: -1h)'
#tables = client.query_api().query(query, org=org)
query_api = client.query_api()

query = 'from(bucket:"sunghunyoon")\
|> range(start: -10m)'

result = client.query_api().query(org=org, query=query)

results = []
for table in result:
    for record in table.records:
        results.append((record.get_value(), record.get_field()))

print(results)