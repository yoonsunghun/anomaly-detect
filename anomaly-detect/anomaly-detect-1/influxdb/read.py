from datetime import datetime

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

# You can generate a Token from the "Tokens Tab" in the UI
token = "SAb_WS8scMjlYu1liSBOFwjmSdPyLSo9B0VrQ8sL_Path4rAZ6aPFOaYahost32FQqKZmU4__EggagfsG9HcwA=="
org = "admin"
bucket = "sunghunyoon"

client = InfluxDBClient(url="http://172.26.16.108:28086", token=token)

query_api = client.query_api()

query = 'from(bucket:"sunghunyoon")\
|> range(start: -10m)'

result = client.query_api().query(org=org, query=query)

results = []
for table in result:
    for record in table.records:
        result.append((record.get_value(), record.get_field()))

print(result)