import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import RobustScaler, MinMaxScaler, StandardScaler, MaxAbsScaler, Normalizer
from keras.layers import LSTM, SimpleRNN, GRU
from keras.models import Sequential, Model
from keras.models import load_model
from keras.layers import Dense, Dropout, Flatten, Input
from keras.layers.merge import concatenate 
import keras.backend as K 
from keras.callbacks import EarlyStopping
from influxdb import InfluxDBClient
import datetime
from datetime import timezone
import os
import sys
import redis
import time

# 1. InfluxDB cpk list 획득 및 1개씩 뽑기 + 다 뽑아서 학습이 끝나는 거 까지 확인 필요 (cron 주기에 있는)
# 리스트 획득 함수
start_date = sys.argv[1] + ' 23:50:00'
print('1.'+start_date)
#end_date = sys.argv[2]+' '+sys.argv[4]
end_date = sys.argv[2] + ' 00:00:00'
print('2.'+end_date)
host_influxdb = "40.117.168.127"
port_influxdb = "8086"
username_influxdb = ""
password_influxdb = ""
database_influxdb = "data001_1"
_format = '%Y-%m-%d %H:%M:%S'
host_redis="40.117.168.127"
port_redis=6379
db_redis=0
ns = 1000000000
period = 60*1
path = "/home/adminuser/anomaly-detect/anomaly-detect-1/"

def convert_datetime (date):
    if(type(date)==str):
        date = datetime.datetime.strptime(date, _format) # datetype 변경
    date_unix = date.replace(tzinfo=timezone.utc).timestamp()*ns # KST > UTC 변경, unixtime으로 변경
    date_unix = int(date_unix) # 지수 형태 > 전체 표기로 표기 형식 변경
    date_str = str(date_unix) # InfluxDB에서 사용하기 위해 다시 string으로 변경

    #print(start_date_unix, end_date_unix)
    return date_str # unixtime string 으로 리턴

def cpk_list_get (end_date):
    # 인플럭스 디비에서 cpk list만 가져오는 쿼리
    # 리스트에 저장
    client = InfluxDBClient(host_influxdb, port_influxdb, username_influxdb, password_influxdb, database_influxdb)
    #query = 'SHOW TAG values WITH KEY = "cpk" LIMIT 1000 OFFSET 1'
    query = 'SELECT * FROM "logstash" WHERE '+ str((int(end_date) - period*ns)) + '<= "time" and "time" <=' + end_date
    print(query)
    result = client.query(query)
    if (len(result) == 0) :
        print("No Cpk")
        quit()
    row_lst = pd.DataFrame(result.get_points())
    cpk_list = row_lst['cpk'].to_list()
    # print(row_lst[0]['value'])
    # print(len(row_lst))
    # cpk_list = []
    # for i in row_lst:
    #     cpk_list.append(i['value'])
    cpk_list = list(set(cpk_list))
    return cpk_list

def process () :
    rd = redis.StrictRedis(host_redis, port_redis, db_redis, decode_responses=True)
    cnt = rd.get("cnt_anomaly")
    print('count ' + cnt)
    if cnt < '5' :
        return 
    else :
        print("sleep")
        time.sleep(10)
        process()

def anomaly (start_date, end_date, cpk) :
    # 참고용 모델을 어떻게 불러올것인지
    # 테스트 셋 불러와야함.
    #print(start_date, end_date)
    #start_date, end_date = convert_unixtime(start_date, end_date)

    model = load_model(path+"model_{}.h5".format(cpk))

    client = InfluxDBClient(host_influxdb, port_influxdb, username_influxdb, password_influxdb, database_influxdb)
    query = 'SELECT sum("hit") FROM "logstash" WHERE ("cpk"=\''+ cpk +  '\') and '  + start_date + '<= "time" and "time" <=' + end_date + ' GROUP BY time(1m) fill(0)'
    print(query)
    result = client.query(query)
    row_lst = list(result.get_points())
    data = pd.DataFrame(row_lst)
    time = data['time'].iloc[-1]
    print ('time ' + time)
    print(data)
    print(len(data))
    #fdf = data.copy()

    # data의 @timestamp 열 데이트 타입 형식으로 변환
    # @timestamp 열 인덱스로 설정 및 기존 @timestamp 열 삭제
    data=data.rename(columns={'time':'@timestamp', 'sum':cpk})
    data['@timestamp'] = pd.to_datetime(data['@timestamp'])
    data.index = data['@timestamp']
    data=data.drop(['@timestamp'],axis=1)
    print(data)

    # 학습기의 end_date 를 가져옴
    query1 = 'SELECT end_date_learning FROM "end_date_learning" WHERE "cpk"=\''+ cpk + '\' ORDER BY desc LIMIT 1'
    print(query1)
    result1 = client.query(query1)
    print(result1)        
    row_lst1 = list(result1.get_points())
    print(row_lst1[0]['end_date_learning'])
    edl = row_lst1[0]['end_date_learning']

    # train : test = 7 : 3 비율로 데이터 분할
    # train, test = train_test_split(data, test_size=0.3, random_state=1, shuffle=False)
    test = data.values
    test_lst = data[cpk].tolist()

    # 딥러닝 모델이 학습을 잘하기 위해서는 정규화 해주는 작업이 필요
    # 각 Feature의 값을 일정한 범위 또는 규칙에 따르게 하기 위해서 스케일링 사용
    # RobustScaler :    중앙값(median)과 IQR(interquartile range) 사용. 아웃라이어의 영향을 최소화
    # scaler = RobustScaler()
    # scaler = MinMaxScaler()
    scaler = StandardScaler()
    # scaler = MaxAbsScaler()
    # scaler = Normalizer()

    test_scaled = scaler.fit_transform(test)

    # 정규화된 train, test 데이터로 dataframe 생성
    test_sc_df = pd.DataFrame(test_scaled, columns=[cpk], index=data.index)
    print(test_sc_df.head())
    
    index = data.index

    test_value = test_sc_df.values


    y_pred = model.predict(test_value, batch_size=1)
    print(y_pred)
    y_pred_org = scaler.inverse_transform(y_pred)   # 정규화된 값을 원래의 크기로 되돌린다.
    print(y_pred_org)

    print("y_pred", y_pred.shape)

    y_total = 0
    y_ok = 0
    ## 표본의 통계량(estimator) 추정
    mean_y_pred = np.mean(y_pred)
    std_y_pred = np.std(y_pred)
    print(std_y_pred)

    ## 상한, 하한 추정(95% 구간 z=1.645)
    ## 상한, 하한 추정(95% 구간 z=1.96)
    ## 상한, 하한 추정(99% 구간 z=2.58)
    z_score = 2.58*(std_y_pred/np.sqrt(len(y_pred_org)))
    # z_score = 1.645*std_y_pred
    #L_ = mean_y_pred - z_score
    #U_ = mean_y_pred + z_score
    #print("L_ ", L_)
    #print("U_ ", U_)
    #print("z_score ", z_score)


    pred_lst = list()
    for n in range(len(y_pred)):
        y_total += 1
        #y_min = y_pred_org[n] - 3*z_score
        #y_max = y_pred_org[n] + 3*z_score
        std_up = mean_y_pred + 3*std_y_pred
        std_down = mean_y_pred - 3*std_y_pred
        #if (test_lst[n] > y_min and test_lst[n] < y_max):
        if (y_test[n] > std_down and y_test[n] < std_up):
            y_ok += 1
        else:
            pred_lst.append([str(index[n]), float(test_lst[n])])
    print("y_total: ", y_total, " y_ok: ", y_ok)
    acc = y_ok / y_total
    print("acc: ", acc)

    plt.figure(figsize=(10, 7))
    plt.plot(test_value, marker='o', markersize='0.5', label='actual')
    plt.plot(y_pred, marker='o', markersize='0.5', label='prediction')
    plt.legend()
    plt.show()
    plt.savefig('model_{}.png'.format(cpk))

    pred_dt = pd.DataFrame(pred_lst, columns=['@timestamp', 'actual_value'])
    pred_dt['@timestamp'] = pd.to_datetime(pred_dt['@timestamp'], utc=True)
    pred_dt = pred_dt.set_index('@timestamp')

    anomaly_df = pd.concat([pred_dt, data], join='outer', axis=1).dropna()
    anomaly_df = anomaly_df.drop('actual_value', axis=1)
    anomaly_df = anomaly_df.reset_index()
    print(anomaly_df)

    """
    @@ Write
    python --> Influxdb
    """
    lst = list()
    for n in range(len(anomaly_df)):
        ts = anomaly_df.iloc[n][0]
        #print(ts)
        ts2 = convert_datetime(ts)
        #print(ts2)
        #print(edl)
        val = int(anomaly_df.iloc[n][1])
        if(int(start_date)<=int(edl)):
            #print(edl)
            if (ts2>=edl):
                print('a')
                print(ts, val)
                lst.append({
                    "measurement": "anomaly",
                    "tags": {
                        "cpk": cpk,
                    },
                    "time": ts,
                    "fields": {"anomaly": val}
                    })
            else:
                continue
        else:
            print('b')
            print(ts, val)
            lst.append({
                "measurement": "anomaly",
                "tags": {
                    "cpk": cpk,
                },
                "time": ts,
                "fields": {"anomaly": val}
                })

    client.write_points(lst)



start_date = convert_datetime (start_date)
end_date = convert_datetime (end_date)
cpk_list = cpk_list_get (end_date)
print(cpk_list)
#print(a)

rd = redis.StrictRedis(host_redis, port_redis, db_redis, decode_responses=True)

process()
rd.get("cnt_anomaly")
rd.incr("cnt_anomaly") # redis +1
for i in cpk_list:
    if os.path.isfile(path+"model_{}.h5".format(i)) :
        print("Yes File")
        anomaly(start_date, end_date, i)
    else :
        print("No file")
        continue

rd.decr("cnt_anomaly")
