## 데이터 분석을 위한 모듈(pandas, matplotlib, train_test_split) 불러오기
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import RobustScaler, MinMaxScaler, StandardScaler, MaxAbsScaler, Normalizer
from keras.layers import LSTM, SimpleRNN, GRU
from keras.models import Sequential 
from keras.layers import Dense, Dropout, Flatten
import keras.backend as K 
from keras.callbacks import EarlyStopping
from influxdb import InfluxDBClient
from tensorflow.keras.models import load_model

def create_X_Y(ts: list, lag: int) -> tuple:
    """
    A method to create X and Y matrix from a time series list for the training of 
    deep learning models 
    """
    X = []

    if len(ts) - lag <= 0:
        X.append(ts)
    else:
        for i in range(len(ts) - lag):
            # y.append(ts[i + lag])
            X.append(ts[i:(i + lag)])

    X = np.array(X)

    # Reshaping the X array to an LSTM input shape 
    X = np.reshape(X, (X.shape[0], X.shape[1], 1))

    return X


cpk = 'megastudyedu'

## test데이터
client = InfluxDBClient(host='40.117.168.127', port=8086, username='', password='', database='data01_1')
result = client.query('SELECT * FROM "logstash" WHERE time > \'2021-02-28T23:59:59Z\' and time < \'2021-03-03T00:00:00Z\' and cpk = \''+cpk+'\'')
row_lst = list(result.get_points())
data2 = pd.DataFrame(row_lst)
# print(data)
fdf2 = data2.copy()

# cpk = data2.iloc[0, 1]
# print(cpk)

# data의 @timestamp 열 데이트 타입 형식으로 변환
# @timestamp 열 인덱스로 설정 및 기존 @timestamp 열 삭제
data2=data2.rename(columns={'time':'@timestamp', 'hit':cpk})
data2=data2.drop(['cpk'], axis=1) # cpk 여러 개 할 때 수정 필요 pd.pivot_table 필요 예상
data2['@timestamp'] = pd.to_datetime(data2['@timestamp'])
data2.index = data2['@timestamp']
data2=data2.drop(['@timestamp'],axis=1)
print(data2)

test_lst = data2[cpk].tolist()

scaler = RobustScaler()
# scaler = MinMaxScaler()
# scaler = StandardScaler()
# scaler = MaxAbsScaler()
# scaler = Normalizer()

data_scaled = scaler.fit_transform(data2)
data_df = pd.DataFrame(data_scaled, columns=[cpk], index=data2.index)
data_val = data_df.values
# data_t = data_val.reshape(data_val.shape[0], 1)

data_y = data_df[cpk].tolist()
data_X = create_X_Y(data_y, 60)


model = load_model('model.h5')
# print(model.summary())

y_pred = model.predict(data_X, batch_size=32)
print(y_pred)
y_pred_org = scaler.inverse_transform(y_pred)   # 정규화된 값을 원래의 크기로 되돌린다.
print(y_pred_org)

y_total = 0
y_ok = 0
# ## 표본의 통계량(estimator) 추정
# mean_y_pred = np.mean(y_pred_org)
# std_y_pred = np.std(y_pred_org)

# ## 상한, 하한 추정(95% 구간 z=1.96)
# L_ = mean_y_pred - 1.96*(std_y_pred/np.sqrt(100))
# U_ = mean_y_pred + 1.96*(std_y_pred/np.sqrt(100))

for n in range(len(y_pred)):
    y_total += 1
    if test_lst[n] > y_pred_org[n]-50 and test_lst[n] < y_pred_org[n]+50:
        y_ok += 1
print("y_total: ", y_total, " y_ok: ", y_ok)
acc = y_ok / y_total

print("acc: ", acc)
plt.figure(figsize=(10, 7))
plt.plot(data_val, marker='o', markersize='0.5', label='actual')
plt.plot(y_pred, marker='o', markersize='0.5', label='prediction')
plt.legend()
plt.show()
# plt.savefig('model_plot.png')



# # 이상치가 발견된 timestamp만을 추출하여 df형태로 저장
# pred_lst = list()
# for n in range(len(y_pred)):
#   diff = abs(y_pred[n] - data_val[n])
#   if (diff > 1):
#     # print(y_test[n])
#     # print(index[n])
#     pred_lst.append([str(data_df.index[n]), float(data_val[n])])

# pred_dt = pd.DataFrame(pred_lst, columns=['@timestamp', 'actual_value'])
# pred_dt['@timestamp'] = pd.to_datetime(pred_dt['@timestamp'])
# pred_dt = pred_dt.set_index('@timestamp')

# anomaly_df = pd.concat([pred_dt, data2], join='outer', axis=1).dropna()
# anomaly_df = anomaly_df.drop('actual_value', axis=1)
# anomaly_df = anomaly_df.reset_index()
# # data_df.append(anomaly_df)
# print(anomaly_df)

