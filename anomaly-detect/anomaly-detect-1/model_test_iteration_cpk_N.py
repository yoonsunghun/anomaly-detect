## 데이터 분석을 위한 모듈(pandas, matplotlib, train_test_split) 불러오기
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

from keras.layers import LSTM 
from keras.models import Sequential 
from keras.layers import Dense, Dropout
import keras.backend as K 
from keras.callbacks import EarlyStopping

# 데이터 불러오기 - pandas의 read_csv 함수 이용
data = pd.read_csv('./test2.csv')# 전처리된 상태의 1개의 고객 회사 완전 가상 데이터 test 파일 불러오기


# data의 @timestamp 열 데이트 타입 형식으로 변환
# @timestamp 열 인덱스로 설정 및 기존 @timestamp 열 삭제
data['@timestamp'] = pd.to_datetime(data['@timestamp'])
data.index = data['@timestamp']
data=data.drop(['@timestamp'],axis=1)


# cpk 이름을 리스트에 저장
# 리스트에서 하나씩 받아서 df 생성.. >> 이걸 변수로 가져올 수 있나...
cpk_col =  list(data.columns)   # 데이터프레임 생성 위한 열 목록(cpk 이름들)
cpk_df = list()                 # 생성된 각각의 데이터프레임이 저장될 리스트
cpk_model = list()
for cpk in cpk_col:
  temp = data[cpk].values
  globals()['df_{}'.format(cpk)] = pd.DataFrame(temp, columns=[cpk], index=data.index)
  cpk_df.append(globals()['df_{}'.format(cpk)])



for df in cpk_df:
  col = str(df.columns.values)
  print(col)
  # 3월1일 7시 기준으로 train, test 분리
  split_date = pd.Timestamp('2021-03-01 07:00:00') # train, test 나눌 기준 지정
  globals()['train_{}'.format(col)] = df.loc[:split_date, df.columns]
  globals()['test_{}'.format(col)] = df.loc[split_date:, df.columns]

  scaler = MinMaxScaler()
  globals()['train_scaled_{}'.format(col)] = scaler.fit_transform(globals()['train_{}'.format(col)])
  globals()['test_scaled_{}'.format(col)] = scaler.fit_transform(globals()['test_{}'.format(col)])

  # 정규화된 train, test 데이터로 dataframe 생성
  globals()['train_sc_df_{}'.format(col)] = pd.DataFrame(globals()['train_scaled_{}'.format(col)], columns=[col], index=[globals()['train_{}'.format(col)].index])
  globals()['test_sc_df_{}'.format(col)] = pd.DataFrame(globals()['test_scaled_{}'.format(col)], columns=[col], index=[globals()['test_{}'.format(col)].index])

  # sliding window 구성
  for s in range(1, 61):
    globals()['train_sc_df_{}'.format(col)]['shift_{}'.format(s)] = globals()['train_sc_df_{}'.format(col)][col].shift(s)
    globals()['test_sc_df_{}'.format(col)]['shift_{}'.format(s)] = globals()['test_sc_df_{}'.format(col)][col].shift(s)


  # X_train, X_test에서 NaN있는 행 제거 후 y로 설정할 값 제거
  globals()['X_train_{}'.format(col)] = globals()['train_sc_df_{}'.format(col)].dropna().drop(col, axis=1)
  globals()['y_train_{}'.format(col)] = globals()['train_sc_df_{}'.format(col)].dropna()[[col]]
  globals()['X_test_{}'.format(col)] = globals()['test_sc_df_{}'.format(col)].dropna().drop(col, axis=1)
  globals()['y_test_{}'.format(col)] = globals()['test_sc_df_{}'.format(col)].dropna()[[col]]
  
  # test에서 validation 값 분리(test 0.8, validation 0.2)
  globals()['X_test_{}'.format(col)],globals()['X_val_{}'.format(col)], \
  globals()['y_test_{}'.format(col)], globals()['y_val_{}'.format(col)] = train_test_split(globals()['X_test_{}'.format(col)], globals()['y_test_{}'.format(col)],
                                                                                           test_size = 0.2, random_state = 3)

  # dataframe 타입이었던 훈련/테스트 데이터들을 values 를 사용하여 dataframe의 numpy 표현형으로 변경
  globals()['X_train_{}'.format(col)] = globals()['X_train_{}'.format(col)].values
  globals()['X_test_{}'.format(col)]= globals()['X_test_{}'.format(col)].values
  globals()['y_train_{}'.format(col)] = globals()['y_train_{}'.format(col)].values
  globals()['y_test_{}'.format(col)] = globals()['y_test_{}'.format(col)].values
  globals()['X_val_{}'.format(col)] = globals()['X_val_{}'.format(col)].values
  globals()['y_val_{}'.format(col)] = globals()['y_val_{}'.format(col)].values
  # print(globals()['X_train_{}'.format(col)].shape)
  # print(globals()['X_test_{}'.format(col)].shape)
  # print(globals()['X_val_{}'.format(col)].shape)

  # LSTM의 input으로 사용하기 위해 2차원인 X_train, X_test, X_val 값을 3차원으로 수정
  # [data_size, time_steps, features]
  globals()['X_train_t_{}'.format(col)] = globals()['X_train_{}'.format(col)].reshape(globals()['X_train_{}'.format(col)].shape[0], 60, 1)
  globals()['X_test_t_{}'.format(col)] = globals()['X_test_{}'.format(col)].reshape(globals()['X_test_{}'.format(col)].shape[0], 60, 1)
  globals()['X_val_t_{}'.format(col)] = globals()['X_val_{}'.format(col)].reshape(globals()['X_val_{}'.format(col)].shape[0], 60, 1)
  
  # print("최종 DATA")
  # print(globals()['X_train_t_{}'.format(col)].shape)
  
  K.clear_session()
  
  # 상태유지 순환신경망 모델 생성
  # ‘stateful=True’옵션을 사용하여 상태유지 가능한 순환신경망 모델을 구성
  globals()['model_{}'.format(col)] = Sequential()
  globals()['model_{}'.format(col)].add(LSTM(32, batch_input_shape=(1, 60, 1))) # batch_input_shape = (배치사이즈, 타임스텝, 속성) 
  # globals()['model_{}'.format(col)].add(LSTM(32, batch_input_shape=(1, 60, 1), stateful=True)) # batch_input_shape = (배치사이즈, 타임스텝, 속성) 
  globals()['model_{}'.format(col)].add(Dense(1)) # output = 1 
  globals()['model_{}'.format(col)].compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])
  globals()['model_{}'.format(col)].summary()
  
  early_stop = EarlyStopping(monitor='loss', patience=1, verbose=1)
  globals()['history_{}'.format(col)] = globals()['model_{}'.format(col)].fit(globals()['X_train_t_{}'.format(col)], globals()['y_train_{}'.format(col)],
                                                                              validation_data = (globals()['X_val_t_{}'.format(col)], globals()['y_val_{}'.format(col)]), 
                                                                              epochs=50, shuffle=False, batch_size=1, verbose=1, callbacks=[early_stop])

  # 모델 평가
  globals()['train_acc_{}'.format(col)] = globals()['model_{}'.format(col)].evaluate(globals()['X_train_t_{}'.format(col)], globals()['y_train_{}'.format(col)],  batch_size=1, verbose=0)
  globals()['test_acc_{}'.format(col)] = globals()['model_{}'.format(col)].evaluate(globals()['X_test_t_{}'.format(col)],globals()['y_test_{}'.format(col)], batch_size=1, verbose=0)
  # print(globals()['train_acc_{}'.format(col)])
  # print(globals()['test_acc_{}'.format(col)])

  # #acc
  plt.plot(globals()['history_{}'.format(col)].history['accuracry']) #train set
  plt.plot(globals()['history_{}'.format(col)].history['val_accuracy'])
  
  plt.title('Accuracy')
  plt.xlabel('epoch')
  plt.ylabel('acc')
  plt.legend(['train', 'val'], loc='upper left')
  plt.show()
  
  #loss
  plt.plot(globals()['history_{}'.format(col)].history['loss']) #train set
  plt.plot(globals()['history_{}'.format(col)].history['val_loss'])
  
  plt.title('Loss')
  plt.xlabel('epoch')
  plt.ylabel('acc')
  plt.legend(['train', 'val'], loc='upper left')
  plt.show()

  cpk_model.append(globals()['model_{}'.format(col)])

  y_pred = globals()['model_{}'.format(col)].predict(globals()['X_test_t_{}'.format(col)], batch_size=1)
  plt.figure(figsize=(8, 5))
  plt.plot(globals()['y_test_{}'.format(col)], label='actual')
  plt.plot(y_pred, label='prediction')
  plt.legend()
  plt.show()


print(cpk_model)